﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallMgrSystem.Models;

namespace CallMgrSystem.Controllers
{
    public class DevicesController : Controller
    {
        private CallSystemEntities db = new CallSystemEntities();

        // GET: Devices
        public ActionResult Index()
        {
            return View(db.tbl_Devices.ToList());
        }

        // GET: Devices/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Devices tbl_Devices = db.tbl_Devices.Find(id);
            if (tbl_Devices == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Devices);
        }

        // GET: Devices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Devices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SiteID,Number,Name,Type,Description,Monitored,MonitoringBegin,CrossrefID,ServiceCrossRefID,MonitorStartInvokeID,MonitorStopInvokeID,DeviceSnapshotInvokeID,AgentStateInvokeID,Status,StatusBegin,LastStatusUpdate,CallID,AgentNumber,LastSelected,SetAgentUnavailable,CarrierID,BillingPolicyID,RecordingType,RecorderID,RecorderChannel,RecordingCode,TimeSlot,DIOPort")] tbl_Devices tbl_Devices)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Devices.Add(tbl_Devices);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Devices);
        }

        // GET: Devices/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Devices tbl_Devices = db.tbl_Devices.Find(id);
            if (tbl_Devices == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Devices);
        }

        // POST: Devices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SiteID,Number,Name,Type,Description,Monitored,MonitoringBegin,CrossrefID,ServiceCrossRefID,MonitorStartInvokeID,MonitorStopInvokeID,DeviceSnapshotInvokeID,AgentStateInvokeID,Status,StatusBegin,LastStatusUpdate,CallID,AgentNumber,LastSelected,SetAgentUnavailable,CarrierID,BillingPolicyID,RecordingType,RecorderID,RecorderChannel,RecordingCode,TimeSlot,DIOPort")] tbl_Devices tbl_Devices)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Devices).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Devices);
        }

        // GET: Devices/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Devices tbl_Devices = db.tbl_Devices.Find(id);
            if (tbl_Devices == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Devices);
        }

        // POST: Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            tbl_Devices tbl_Devices = db.tbl_Devices.Find(id);
            db.tbl_Devices.Remove(tbl_Devices);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
