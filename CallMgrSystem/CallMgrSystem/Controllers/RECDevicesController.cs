﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallMgrSystem.Models;

namespace CallMgrSystem.Controllers
{
    public class RECDevicesController : Controller
    {
        private CallViewEntities db = new CallViewEntities();

        // GET: RECDevices
        public ActionResult Index()
        {
            return View(db.vw_RECDevices.ToList());
        }

        public ActionResult Test()
        {
            string salesFTPPath = "D:/IVG/fileaudio";
            DirectoryInfo salesFTPDirectory = new DirectoryInfo(salesFTPPath);
            IEnumerable<string> files = salesFTPDirectory.GetFiles()
              .Where(f => f.Extension == ".mp3" || f.Extension == ".wav" )
              .OrderBy(f => f.Name)
              .Select(f => f.FullName);
            return Json(files, JsonRequestBehavior.AllowGet);
        }

        // GET: RECDevices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECDevices vw_RECDevices = db.vw_RECDevices.Find(id);
            if (vw_RECDevices == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECDevices);
        }

        // GET: RECDevices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RECDevices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Trạm,Số_máy,Tên,Kiểu_ghi,Mã_ghi,Trạng_thái,Thời_lượng,Chủ_gọi,Bị_gọi,Mô_tả,Status,RecordingFile,SiteID")] vw_RECDevices vw_RECDevices)
        {
            if (ModelState.IsValid)
            {
                db.vw_RECDevices.Add(vw_RECDevices);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vw_RECDevices);
        }

        // GET: RECDevices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECDevices vw_RECDevices = db.vw_RECDevices.Find(id);
            if (vw_RECDevices == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECDevices);
        }

        // POST: RECDevices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Trạm,Số_máy,Tên,Kiểu_ghi,Mã_ghi,Trạng_thái,Thời_lượng,Chủ_gọi,Bị_gọi,Mô_tả,Status,RecordingFile,SiteID")] vw_RECDevices vw_RECDevices)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vw_RECDevices).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vw_RECDevices);
        }

        // GET: RECDevices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECDevices vw_RECDevices = db.vw_RECDevices.Find(id);
            if (vw_RECDevices == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECDevices);
        }

        // POST: RECDevices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            vw_RECDevices vw_RECDevices = db.vw_RECDevices.Find(id);
            db.vw_RECDevices.Remove(vw_RECDevices);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
