function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("f5devTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc"; 
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;      
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
//Input Date
$(document).ready(function(){
  moment.locale('vi');
  //var ahmet = moment("25/04/2012","DD/MM/YYYY").year();
  var date = new Date();
  bugun = moment(date).format("DD/MM/YYYY");
  var date_input=$('input[name="date"]'); //our date input has the name "date"
  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
  var options={
  //startDate: '+1d',
  //endDate: '+0d',
  container: container,
  todayHighlight: true,
  autoclose: true,
  format: 'dd/mm/yyyy',
  language: 'vi',
  //defaultDate: moment().subtract(15, 'days')
  //setStartDate : "<DATETIME STRING HERE>"
  };
  date_input.val(bugun);
  date_input.datepicker(options).on('focus', function(date_input){
  $("h5").html("focus event");
  }); ;


  date_input.change(function () {
  var deger = $(this).val();
  $("h5").html("<font color=green>" + deger + "</font>");
  });



  $('.input-group').find('.glyphicon-calendar').on('click', function(){
  //date_input.trigger('focus');
  //date_input.datepicker('show');
  //$("h3").html("event : click");
  if( !date_input.data('datepicker').picker.is(":visible"))
  {
  date_input.trigger('focus');
  $("h5").html("Ok");

  //$('.input-group').find('.glyphicon-calendar').blur();
  //date_input.trigger('blur');
  //$("h3").html("görünür");
  } else {
  }
  });


  });