﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallMgrSystem.Models;

namespace CallMgrSystem.Controllers
{
    public class RECCallHistoryController : Controller
    {
        private CallViewEntities db = new CallViewEntities();

        // GET: RECCallHistory
        public ActionResult Index()
        {
            return View(db.vw_RECCallHistory.ToList());
        }

        // GET: RECCallHistory/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECCallHistory vw_RECCallHistory = db.vw_RECCallHistory.Find(id);
            if (vw_RECCallHistory == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECCallHistory);
        }

        // GET: RECCallHistory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RECCallHistory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Trạm,Số_máy,Bắt_đầu,Chủ_gọi,Bị_gọi,Thời_lượng,Kết_thúc,Ghi_chú,Duration,Direction,RecordingFile")] vw_RECCallHistory vw_RECCallHistory)
        {
            if (ModelState.IsValid)
            {
                db.vw_RECCallHistory.Add(vw_RECCallHistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vw_RECCallHistory);
        }

        // GET: RECCallHistory/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECCallHistory vw_RECCallHistory = db.vw_RECCallHistory.Find(id);
            if (vw_RECCallHistory == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECCallHistory);
        }

        // POST: RECCallHistory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Trạm,Số_máy,Bắt_đầu,Chủ_gọi,Bị_gọi,Thời_lượng,Kết_thúc,Ghi_chú,Duration,Direction,RecordingFile")] vw_RECCallHistory vw_RECCallHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vw_RECCallHistory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vw_RECCallHistory);
        }

        // GET: RECCallHistory/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vw_RECCallHistory vw_RECCallHistory = db.vw_RECCallHistory.Find(id);
            if (vw_RECCallHistory == null)
            {
                return HttpNotFound();
            }
            return View(vw_RECCallHistory);
        }

        // POST: RECCallHistory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            vw_RECCallHistory vw_RECCallHistory = db.vw_RECCallHistory.Find(id);
            db.vw_RECCallHistory.Remove(vw_RECCallHistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
