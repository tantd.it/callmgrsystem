﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallMgrSystem.Models;

namespace CallMgrSystem.Controllers
{
    public class RolesController : Controller
    {
        private CallSystemEntities db = new CallSystemEntities();

        // GET: Roles
        public ActionResult Index()
        {
            return View(db.tbl_Roles.ToList());
        }

        // GET: Roles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roles tbl_Roles = db.tbl_Roles.Find(id);
            if (tbl_Roles == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Roles);
        }

        // GET: Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Role,Description,Status,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")] tbl_Roles tbl_Roles)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Roles.Add(tbl_Roles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Roles);
        }

        // GET: Roles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roles tbl_Roles = db.tbl_Roles.Find(id);
            if (tbl_Roles == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Roles);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Role,Description,Status,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")] tbl_Roles tbl_Roles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Roles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Roles);
        }

        // GET: Roles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Roles tbl_Roles = db.tbl_Roles.Find(id);
            if (tbl_Roles == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Roles);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Roles tbl_Roles = db.tbl_Roles.Find(id);
            db.tbl_Roles.Remove(tbl_Roles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
