﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallMgrSystem.Models;

namespace CallMgrSystem.Controllers
{
    public class SitesController : Controller
    {
        private CallSystemEntities db = new CallSystemEntities();

        // GET: Sites
        public ActionResult Index()
        {
            return View(db.tbl_Sites.ToList());
        }

        // GET: Sites/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Sites tbl_Sites = db.tbl_Sites.Find(id);
            if (tbl_Sites == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Sites);
        }

        // GET: Sites/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,HostType,PBXType,IPAddress,Port,RoutingCode,Enable,LastHeartbeatTime,Description,LogCSTA,LogLevel,ForceLogOff,CDRType,CDRIPAddress,CDRPort,CDRPathIn,CDRPathOut,CDRDayStart,CDRDayLength,CDRMonthStart,CDRMonthLength,CDRYearStart,CDRYearLength,CDRHourStart,CDRHourLength,CDRMinuteStart,CDRMinuteLength,CDRSecondStart,CDRSecondLength,CDRCallingStart,CDRCallingLength,CDRCalledStart,CDRCalledLength,CDRTalkHourStart,CDRTalkHourLength,CDRTalkMinStart,CDRTalkMinLength,CDRTalkSecStart,CDRTalkSecLength,CDRTrunkStart,CDRTrunkLength,CDRAccountStart,CDRAccountLength,CDRRouteStart,CDRRouteLength,CDRSeizureStart,CDRSeizureLength,CDRSitePrefix,CDRTimeIsCallBegin,CDRStartOfLine,CDREndOfLine,CDRHTTPGet,CDRHTTPDelete,CDRTFTPFile,CDRRingStart,CDRRingLength,CDRHoldStart,CDRHoldLength,RecordPath,MaxStorageDays,MinPctFreeDiskSize,AMOHost,AMOPort,AMOLogin,AMOPassw")] tbl_Sites tbl_Sites)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Sites.Add(tbl_Sites);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Sites);
        }

        // GET: Sites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Sites tbl_Sites = db.tbl_Sites.Find(id);
            if (tbl_Sites == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Sites);
        }

        // POST: Sites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,HostType,PBXType,IPAddress,Port,RoutingCode,Enable,LastHeartbeatTime,Description,LogCSTA,LogLevel,ForceLogOff,CDRType,CDRIPAddress,CDRPort,CDRPathIn,CDRPathOut,CDRDayStart,CDRDayLength,CDRMonthStart,CDRMonthLength,CDRYearStart,CDRYearLength,CDRHourStart,CDRHourLength,CDRMinuteStart,CDRMinuteLength,CDRSecondStart,CDRSecondLength,CDRCallingStart,CDRCallingLength,CDRCalledStart,CDRCalledLength,CDRTalkHourStart,CDRTalkHourLength,CDRTalkMinStart,CDRTalkMinLength,CDRTalkSecStart,CDRTalkSecLength,CDRTrunkStart,CDRTrunkLength,CDRAccountStart,CDRAccountLength,CDRRouteStart,CDRRouteLength,CDRSeizureStart,CDRSeizureLength,CDRSitePrefix,CDRTimeIsCallBegin,CDRStartOfLine,CDREndOfLine,CDRHTTPGet,CDRHTTPDelete,CDRTFTPFile,CDRRingStart,CDRRingLength,CDRHoldStart,CDRHoldLength,RecordPath,MaxStorageDays,MinPctFreeDiskSize,AMOHost,AMOPort,AMOLogin,AMOPassw")] tbl_Sites tbl_Sites)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Sites).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Sites);
        }

        // GET: Sites/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Sites tbl_Sites = db.tbl_Sites.Find(id);
            if (tbl_Sites == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Sites);
        }

        // POST: Sites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Sites tbl_Sites = db.tbl_Sites.Find(id);
            db.tbl_Sites.Remove(tbl_Sites);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
