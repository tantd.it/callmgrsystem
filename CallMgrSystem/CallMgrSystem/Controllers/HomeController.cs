﻿using CallMgrSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallMgrSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(tbl_Users user)
        {
            if (ModelState.IsValid)
            {
                using (CallSystemEntities db = new CallSystemEntities())
                {
                    var obj = db.tbl_Users.Where(a => a.userName.Equals(user.userName) && a.Email.Equals(user.Email)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.userName.ToString();
                        Session["UserName"] = obj.Email.ToString();
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(user);
        }

        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}